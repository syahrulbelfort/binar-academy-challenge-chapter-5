const express = require('express');
const router = express.Router();
const passport = require('passport');
const auth = require('../Authentication/auth');

//Login Route and Post
router.get('/', auth.checkNotAuthenticated, (req, res) => {
  res.render('login.ejs');
});

router.post(
  '/',
  passport.authenticate('local', {
    successRedirect: '/games',
    failureRedirect: '/login',
    failureFlash: true,
  })
);
module.exports = router;
