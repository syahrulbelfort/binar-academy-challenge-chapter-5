const express = require('express');
const router = express.Router();
let users = require('../db/users.json');
const auth = require('../Authentication/auth');
const bcrypt = require('bcrypt');

//Register Route & Post
router.get('/', async (req, res) => {
  res.render('register.ejs');
});

router.post('/', auth.checkNotAuthenticated, async (req, res) => {
  try {
    const hashPassword = await bcrypt.hash(req.body.password, 10);
    users.push({
      id: Date.now().toString(),
      name: req.body.name,
      email: req.body.email,
      password: hashPassword,
    });

    res.redirect('/login');
  } catch {
    res.redirect('/');
  }
  console.log(users);
});

module.exports = router;
