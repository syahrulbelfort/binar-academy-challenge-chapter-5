const express = require('express');
const router = express.Router();
const auth = require('../Authentication/auth');


router.get('/', auth.checkAuthenticated, (req, res) => {
  res.render('gameplay.ejs');
});

module.exports = router;
