if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config();
}
const express = require('express');
const port = 3000;
const app = express();
const router = express.Router();
const db = require('./db/users.json');
const passport = require('passport');
const flash = require('express-flash');
const session = require('express-session');
const methodOverride = require('method-override');
const initializePassport = require('./Config/passport-config');
const users = require('./db/users.json');
initializePassport(
  passport,
  (email) => users.find((user) => user.email === email),
  (id) => users.find((user) => user.id === id)
);
app.set('view engine', 'ejs'); //this is a set engine from ejs
app.use(express.json()); // this is to accept data in JSON format
app.use(express.urlencoded({ extended: false })); // Decode the data through HTML form

app.use(flash());
app.use(
  session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
  })
);
app.use(passport.initialize());
app.use(passport.session());
app.use(methodOverride('_method'));

//STATIC FILE
app.use(express.static(__dirname + '/Public/Home')); //this is to serve static Home folder
app.use(express.static(__dirname + '/Public/Gameplay')); //this is to serve static Gameplay folder
//------

//HOME ROUTES
const homeRoute = require('./Routes/homeRoute');
app.use('/', homeRoute);

//GAMEPLAY ROUTES
const gameplayRoute = require('./Routes/gameRoute');
app.use('/games', gameplayRoute);
//----

//REGISTER ROUTES
const registerRoute = require('./Routes/registerRoute.js');
app.use('/register', registerRoute);
//----

//LOGIN ROUTES
const loginRoute = require('./Routes/loginRoute');
app.use('/login', loginRoute);
//----

//LOGOUT ROUTES
const logoutRoute = require('./Routes/logoutRoute');
app.use('/logout', logoutRoute);

//INTERNAL SERVER ERROR
app.use((err, req, res, next) => {
  res.status(500).json({
    status: 'fail',
    errors: err.message,
  });
});

//404 HANDLER
app.use((req, res, next) => {
  res.status(404).json({
    status: 'fail',
    errors: 'Are you lost?',
  });
});

app.listen(port, () => {
  console.log(`server is okay`);
});
